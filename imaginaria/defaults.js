var defaults = {
    characterProperties: {
        dropAllowed: {
            default:false,
            description: "The character can drop things",
        },
        giveAllowed: {
            default:false,
            description: "The character can give things to other characters",
        },
        showWhenInLocation: {
            description: "Text shown when the character is in location",
            default: ""
        },
        showWhenEnteringLocation: {
            description: "Text shown when the character enters location",
            default: ""
        },
        showWhenExitingLocation: {
            description: "Text shown when the character exits location",
            default: ""
        }
    },
    thingProperties: {       
        showWhenUsed: {
            description: "Text shown when the thing is used",
            default: "Nothing happens"
        },
        showWhenInLocation: {
            description: "Text shown when the thing is in location",
            default: ""
        },
        openable: {
            default:false,
            description: "It can be opened or closed",
            subProps: {
                isOpen: {
                    description: "Is it open?",
                    default:true
                },
                lockable: {
                    default: false,
                    description: "It can be locked or unlocked",
                    subProps: {
                        isLocked: {
                            description: "Is it locked?",
                            default:false
                        },
                        unlockWith: {
                            description: "What things can unlock it?",
                            default: {}
                        }
                    }                    
                }
            },
        },
        supporter: {
            default: false,
            description: "It can have things or characters on top",
            subProps: {
                isHolding:{
                    default:{},
                    description: "What things has on top?"
                },
                canHoldCharacters:{
                    default:false,
                    description: "Characters can get on top of it"
                }
            }
        },
        container: {
            default: false,
            description: "It can have things or characters inside",
            subProps: {
                isContaining:{
                    default:{},
                    description: "What things has inside?"
                },
                canContainCharacters:{
                    default:false,
                    description: "Characters can get inside of it"
                },
                seeThrough:{
                    default:false,
                    description: "Things inside are listed when examined"
                }
            }
        },
        carriable: {
            default:false,
            description: "It can picked up and dropped off"
        },
        wearable: {
            default:false,
            description: "It can be put on or taken off"
        },
        drinkable: {
            description: "It can be drank",
            default:false
        },
        edible: {
            description: "It can be eaten",
            default:false
        },
        readable: {
            description: "It can be read",
            default:false,
            subProps:{
                textWhenRead:{
                    description: "Text to display when read",
                    default:""
                }
            }
        },
        switchable: {
            description: "It can be turned on or off",
            default: false,
            subProps:{
                isOn:{
                    description: "Is it on?",
                    default:false
                }
            }
        }

    }
}