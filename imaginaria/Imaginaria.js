var Imaginaria = {
    story: new Story(),
    parser: new Parser(),
    init: function(story){
        this.story.initElements("Command",Dictionary);
        this.story.loadStory(story);
    },
    executeCommand : function(command,gui){
        var match = this.parser.parseCommand(command);
        console.log(match);
        if (match && match.full) {
            console.log(match);
            if (match.command.checkRestrictions(match,gui)){
                var specificCommand = _.find(match.command.subCommands,function(subCommand){
                    return subCommand.paramCondition(match.elements);
                });
                if (!specificCommand){
                    match.command.execute(match.elements,gui);
                } else if (specificCommand.checkRestrictions(match,gui)){
                    specificCommand.command.execute(match.elements,gui);
                }                
            }            
        } else if (match && match.partial){
            match.command.executePartial(match.elements,gui);
        } else {
            gui.showText("What?");    
        }        
    },
    executeEvent : function(eventKey,gui){
        if (_.has(this.story.events,eventKey)){
            this.story.events[eventKey].execute(gui);
        }
    },
    utils: {
        joinList: function(list){
            if (list.length == 1){
                return list[0];
            } else if (things.length != 0) {
                var allButLast = list.slice(0,list.length-1);
                var joinedList = allButLast.join(", ");
                return joinedList+" and "+list[list.length-1];
            };
        }
    }
}



Imaginaria.init(story);
