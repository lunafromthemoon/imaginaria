
function Command(key,commandMeta){
  this.setMetaInfo(key,commandMeta);
  this.subCommands = [];
  this.syntax = Imaginaria.parser.extractPatterns(this.syntax);
}

extend(Command.prototype, HasMetaInfo);
extend(Command.prototype, HasRestrictions);


function SpecificCommand(key,commandMeta){
    this.setMetaInfo(key,commandMeta);
    this.command = Imaginaria.story.commands[this.command];
    this.command.subCommands.push(this);
}

extend(SpecificCommand.prototype, HasMetaInfo);
extend(SpecificCommand.prototype, HasRestrictions);
extend(SpecificCommand.prototype, HasReferences);