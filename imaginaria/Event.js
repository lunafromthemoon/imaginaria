function Event(key,action) {
    this.key = key;
    this.action = action;
    this.executionCount = 0;

    this.execute = function(gui){
        this.action(gui);
        this.executionCount++;
    }

    this.executed = function(){
        return this.executionCount > 0;
    }
}