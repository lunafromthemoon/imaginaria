var HasComplexName = {
    setName : function(){
        this.name = {
            article: this.article,
            adjective: this.adjective,
            noun: this.noun
        };
        this.name.partialName = this.name.adjective ? (this.name.adjective+" "+this.name.noun) : this.name.noun;
        this.name.wholeName = this.article+" "+this.name.partialName;
    },
    getName : function(){
        return this.name.wholeName;
    }
};

var HasLocation = {
    isInLocation : function(location){
        return this.location.context == location.context && this.location.actualLocation.key == location.actualLocation.key;
    },
    moveTo: function(location){
        this.location = {context:"Place", actualLocation: location};
    },
    getInside : function(thing){
        this.location = {context:"inside", actualLocation: thing};
        thing.properties.isContaining[this.key] = this;
    },
    isInside : function(thing){
        return this.location.context == "inside" && this.location.actualLocation.key == thing.key;
    },
    isOnTopOf: function(thing){
        return this.location.context == "on" && this.location.actualLocation.key == thing.key;
    },
    getOnTopOf: function(thing){
        this.location = {context:"on", actualLocation: thing};
        thing.properties.isHolding[this.key] = this;
    },
    getHeldBy: function(character){
        this.location = {context:"heldby", actualLocation: character};
    }
}

var HasDynamicDescription = {
    setDescription : function(){
        this.description = new DynamicDescription(this.description);
    },
    getDescription : function(){
        return this.description.get();
    }
};

var HasProperties = {
    setProperties : function(defaultProperties,customProperties){
        this.properties = this.mapProperties(defaultProperties);
        _.each(customProperties,function(value,key){
            this.properties[key] = value; 
        },this);
    },
    mapProperties : function(props){
        var properties = {};
        _.each(props,function(property,key){
            properties[key] = property.default;
            if (_.has(property,"subProps")){
                _.extend(properties,this.mapProperties(property.subProps));
            }
        },this);
        return properties;
    },
};

var HasGroups = {
    setGroups : function(){
        this.groups = [];
    },
    isMemeberOfGroup : function(group){
        return group.isMember(this);
    }
}

var HasMetaInfo = {
    setMetaInfo : function(key,metaInfo){
        this.key = key;
        _.each(metaInfo,function(property,key){
            this[key] = property;
        },this);
    }    
}

var HasRestrictions = {
    checkRestrictions: function(match,gui){
        var brokenRestriction = _.find(this.restrictions, function(restriction){
            return restriction.condition(match.elements);
        },this);
        if (brokenRestriction){
            brokenRestriction.action(match.elements,match.originalParams,gui);
        }
        return brokenRestriction == undefined;
    }
}

var HasReferences = {
    solveReferences: function(){
        _.each(_.keys(this),function(key){
            this[key] = this.solvePropertyRef(this[key]);
        },this)
    },
    solvePropertyRef: function(property){
        if (_.has(property,"type") && _.has(property,"key")){
            return Imaginaria.story.solveReference(property)
        } else if (property instanceof Array) {
            return this.solveListRef(property);
        } else if ((property instanceof Object) && !(property instanceof Function)){
            return this.solveMapRef(property);
        }
        return property;
    },
    solveMapRef: function(properties){
        var solvedProperties = {};
        _.each(properties,function(property,key){
            solvedProperties[key] = this.solvePropertyRef(property);
        },this);
        return solvedProperties;
    },
    solveListRef: function(properties){
        return _.map(properties,function(property){
            return this.solvePropertyRef(property);
        },this);
    },
}

function extend(destination, source) {
  for (var k in source) {
    if (source.hasOwnProperty(k)) {
      destination[k] = source[k];
    }
  }
  return destination; 
}