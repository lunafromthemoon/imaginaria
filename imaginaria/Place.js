function Place(key, placeMeta){
    this.setMetaInfo(key, placeMeta);
    this.setDescription();
    this.setGroups();
    this.isDesignated = function(designation){
        return designation.test(this.name);
    }

    this.getName = function(){
        return this.name;
    }

/*
    this.solveReferences = function(){
        _.each(this.connections,function(connection,key){
            this.connections[key] = Imaginaria.story.solveReference(connection);
        },this);
    }*/

    this.hasDirection = function(direction){
        return _.find(this.connections,function(connection,key){
            return key == direction;
        }) != undefined;
    }

    this.getLocationTo = function(direction){
        return this.connections[direction];
    }
}

extend(Place.prototype, HasMetaInfo);
extend(Place.prototype, HasDynamicDescription);
extend(Place.prototype, HasReferences);
extend(Place.prototype, HasGroups);