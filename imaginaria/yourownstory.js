var story = {
    characters: {
        you: {
            name:"you",
            article: "",
            adjective: "",
            noun: "",
            properName: "you",
            location: {context:"Place", actualLocation:{type:"Place", key:"whiteRoom"}},
            description:{
                unconditional:"You are yourself, whatever you are",
                conditional: [{
                    condition: function(){
                        return Imaginaria.story.playerCharacter.isHolding("qwer") != undefined;
                    },
                    description: "You are a qwer holder"
                },{
                    condition: function(){
                        return Imaginaria.story.playerCharacter.isHolding("asdf") != undefined;
                    },
                    description: "You are a asdf holder"
                }]
            }
        }
    },
    playerCharacter: "you",
    things: {
        flower: {
            article: "a",
            adjective: "",
            noun: "flower",
            description: {unconditional: "It's a flower floating in the void. Nothing interesting about it."},
            location: {context:"Place", actualLocation:{type:"Place", key:"whiteRoom"}}
        },
        asdf: {
            article: "an",
            adjective: "",
            noun: "asdf",
            description: {unconditional: "You're not sure what this contraption is, but it surely looks like an asdf"},
            location: {context:"inside", actualLocation:{type:"Thing", key:"qwer"}}
        },
        qwer: {
            article: "a",
            adjective: "",
            noun: "qwer",
            description: {unconditional: "That's a qwer, clear as day."},
            location: {context:"Place", actualLocation:{type:"Place", key:"whiteRoom"}},
            customProperties: {
                container:true,
                isContaining:[{type:"Thing", key:"asdf"}],
                seeThrough:true
            }
        }, 
    },
    places: {
        whiteRoom: {
            name: "The White Room",
            description: {unconditional: "You are in a white room, it's all white and there's nothing more to it... \n\n\nExcept for that flower, that is."},
            connections: {
                "south" : {type:"Place",key:"blackRoom"}
            }
        },
        blackRoom: {
            name: "The Black Room",
            description: {unconditional: "The room is all black and you can't see anything else"},
            connections: {
                "north" : {type:"Place",key:"whiteRoom"}
            }
        },
    },
    commands: {
        qwerty : {
            name: "Qwerty",
            category: "misc",
            description: "Does the qwerty",
            restrictions: [],
            syntax: ["qwerty <t:Thing>"],
            execute: function(args,gui){
                gui.showText("You try to qwerty "+args.t.getName()+" but it's impossible.");                
            }
        },
    },
    specificCommands: {
        qwertyQwer : {
            name: "Qwerty the qwer",
            category: "misc",
            description: "Does the qwerty on the qwer",
            restrictions: [],
            paramCondition: function(args){
                return args.t.key == "qwer";
            },
            command:"qwerty",
            execute: function(args,gui){
                gui.showText("You do the real qwerty");
                Imaginaria.executeEvent("qwerEvent",gui);                
            }
        },
    },
    events: {
        start: function(gui){
            gui.showText("Welcome to a shitty game");
        },
        qwerEvent: function(gui){
            gui.showText("Qwer event happening in\n");
            gui.showText("3\n","#FF0000");
            gui.showText("2\n","#0000FF");
            gui.showText("1\n","#00FF00");
            gui.showText("0\nDone!");
        }
    },
    groups: {
        strangeThings: {
            type: "Thing",
            members: ["asdf","qwer"]
        }
    }
}