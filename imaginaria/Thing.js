
function Thing(key,thingMeta){
   
    this.setMetaInfo(key,thingMeta);
    this.setName();
    this.setDescription();
    this.setGroups();
    this.setProperties(defaults.thingProperties,thingMeta.customProperties);

    this.isDesignated = function(designation){
        return designation.test(this.name.noun) 
            || designation.test(this.name.partialName) 
            || designation.test(this.name.wholeName);
    }

/*
    this.solveReferences = function(){
        this.location = Imaginaria.utils.solveLocationRef(this.startingLocation);  
        this.properties = Imaginaria.utils.solvePropertyRef(this.properties);
    }*/
}

extend(Thing.prototype, HasMetaInfo);
extend(Thing.prototype, HasComplexName);
extend(Thing.prototype, HasDynamicDescription);
extend(Thing.prototype, HasProperties);
extend(Thing.prototype, HasReferences);
extend(Thing.prototype, HasGroups);
extend(Thing.prototype, HasLocation);
