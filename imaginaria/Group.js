function Group(key,groupMeta) {
    this.setMetaInfo(key,groupMeta);

    this.members = _.map(this.members,function(member){
        var element = Imaginaria.story.getElement(this.type,member);
        element.groups.push(this);
        return element;
    },this);

    this.isMember = function(element){
        _.find(this.members,function(member){
            return member.key == element.key; 
        },this)
    }
}

extend(Group.prototype, HasMetaInfo);