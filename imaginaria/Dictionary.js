//minimalist dictorionary
/*
move
show inventory
take, drop and give objects
open or close objects
use objects by themselves or combine them
examine surroundings, things and characters
*/ 

var Dictionary = {
    goSomewhere : {
        name: "Go Somewhere",
        category: "movement",
        description: "Moves the player character from one place to another",
        restrictions: [],
        syntax: ["go to <d:Direction>","go <d:Direction>","walk to <d:Direction>","move to <d:Direction>","run to <d:Direction>"],
        execute: function(args,gui){
            Imaginaria.story.playerCharacter.move(args.d);
            gui.showText("You go "+args.d+" and find yourself in "+Imaginaria.story.playerCharacter.location.actualLocation.name);
        },
        executePartial: function(args,gui){
            gui.showText("Where do you want to go?");
        }
    },
    inventory : {
        name: "Inventory",
        category: "inventory",
        description: "Shows the player character inventory",
        restrictions: [],
        syntax : ["inventory","i"],
        execute: function(args,gui){    
            if (Imaginaria.story.playerCharacter.inventory.count() == 0){
                gui.showText("You are not carrying anything");
            } else {
                gui.showText("You are carrying "+Imaginaria.story.playerCharacter.inventory.list());
            }
        }
    },
    pickUp: {
        name: "Pick Up Something",
        category: "inventory",
        description: "Adds a thing to the player character inventory",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.knows(args.t))
                },
                action: function(args,originals,gui) {
                    gui.showText("Pick up what?");
                }
            },
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.canReach(args.t))
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't pick up "+originals.t+", it's not here");
                }
            },
            {
                condition: function(args){
                    return Imaginaria.story.playerCharacter.inventory.has(args.t);
                },
                action: function(args,originals,gui) {
                    gui.showText("You are already have "+originals.t);
                }
            },
            {
                condition: function(args){
                    return !(args.t.carriable)
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't pick "+originals.t+" up");
                }
            }            
        ],
        syntax: [
            "get <t:Thing>",
            "pick up <t:Thing>",
            "pick <t:Thing> up",
            "take <t:Thing>"                
        ],
        execute: function(args,gui){                
            Imaginaria.story.playerCharacter.inventory.add(args.t);
            gui.showText("You pick up "+args.t.getName());
        },
        executePartial: function(args,gui){
            gui.showText("Pick up what?");
        }
    },
    drop: {
        name: "Drop Something",
        category: "inventory",
        description: "Removes a thing to the player character inventory",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.knows(args.t))
                },
                action: function(args,originals,gui) {
                    gui.showText("Drop what?");
                }
            },
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.inventory.has(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("You are not carrying "+originals.t);
                }
            },
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.properties.dropAllowed);
                },
                action: function(args,originals,gui) {
                    gui.showText("You should not drop "+t.getName()+", you might need it later");
                }
            },
        ],
        syntax: [
            "drop <t:Thing>",
            "put down <t:Thing>",
            "put <t:Thing> down",
            "release <t:Thing>"                
        ],
        execute: function(args,gui){
            Imaginaria.story.playerCharacter.inventory.remove(args.t);            
            gui.showText("You drop "+args.t.name.wholeName);
        },
        executePartial: function(args,gui){
            gui.showText("Drop what?");
        }
    }, 
    give: {
        name: "Give Something to Someone",
        category: "inventory",
        description: "Changes a thing from player character inventory to another character inventory",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.inventory.has(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("You are not carrying "+originals.t);
                }
            },
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.properties.giveAllowed);
                },
                action: function(args,originals,gui) {
                    gui.showText("You should not give "+t.getName()+" away, you might need it later");
                }
            },
        ],
        syntax: [
            "give <t:Thing> to <c:Character>",
            "offer <t:Thing> to <c:Character>"            
        ],
        execute: function(args,gui){
            Imaginaria.story.playerCharacter.inventory.remove(args.t);
            args.c.inventory.add(args.t);
            gui.showText("You give "+args.t.getName()+" to "+args.c.getName());
        },
        executePartial: function(args,gui){
            var question = "Give ";
            question = (!_.has(args,"t")) ? "what" : args.t.getName();
            question+=" to "
            question = (!_.has(args,"c")) ? "whom" : args.c.getName();
            question+="?"
            gui.showText(question);
        }
    },
    open:{
        name: "Open Something",
        category: "Things",
        description: "Changes openable thing's open state to true",
        restrictions: [
            {
                condition: function(args){
                    return !(args.t.properties.openable);
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't open "+t.getName());
                }
            },{
                condition: function(args){
                    return args.t.properties.isOpen;
                },
                action: function(args,originals,gui) {
                    gui.showText(t.getName()+" is already open");
                }
            },{
                condition: function(args){
                    return args.t.properties.lockable && args.t.properties.isLocked;
                },
                action: function(args,originals,gui) {
                    gui.showText(t.getName()+" is locked");
                }
            }
        ],
        syntax: ["open <t:Thing>"],
        execute: function(args,gui){
            args.t.properties.isOpen = true;
            gui.showText("You open "+args.t.getName());
        },
        executePartial: function(args,gui){
            gui.showText("Open what?");
        }
    },    
    close:{
        name: "Close Thing",
        category: "Things",
        description: "Changes openable thing's isOpen state to false",
        restrictions: [
            {
                condition: function(args){
                    return !(args.t.properties.openable);
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't close "+t.getName());
                }
            },{
                condition: function(args){
                    return !(args.t.properties.isOpen);
                },
                action: function(args,originals,gui) {
                    gui.showText(t.getName()+" is already closed");
                }
            }
        ],
        syntax: ["close <t:Thing>", "shut <t:Thing>"],
        execute: function(args,gui){
            args.t.properties.isOpen = false;
            gui.showText("You close "+args.t.getName());
        },
        executePartial: function(args,gui){
            gui.showText("Close what?");
        }
    },
    look: {
        name: "Look Around",
        category: "Places",
        description: "Show location description",
        restrictions: [],
        syntax: ["look", "look around"],
        execute: function(args,gui) {
            gui.showText(Imaginaria.story.playerCharacter.location.actualLocation.description.get());
        }
    },
    use: {
        name: "Use Something",
        category: "Things",
        description: "Tries to use a Thing",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.knows(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("Use what?");
                }
            },  
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.canReach(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't use "+originals.t+", it's not here");
                }
            }    
        ],
        syntax: ["use <t:Thing>", "activate <t:Thing>"],
        execute: function(args,gui) {
            gui.showText(args.t.properties.showWhenUsed);
        },
        executePartial: function(args,gui){
            gui.showText("Use what?")
        }
    },
    useWith: {
        name: "Use Something with Something else",
        category: "Things",
        description: "Tries to combine a Thing with another Thing",
        restrictions: [
            {
                condition: function(args){
                    return !(args.t.isInLocation(Imaginaria.story.playerCharacter.location) || Imaginaria.story.playerCharacter.inventory.has(args.t))
                },
                action: function(args,originals,gui) {
                    gui.showText("You don't see any "+originals.t);
                }
            }    
        ],
        syntax: ["use <t1:Thing> with <t2:Thing>", "combine <t1:Thing> with <t2:Thing>"],
        execute: function(args,gui) {
            gui.showText(args.t.properties.showWhenUsed);
        },
        executePartial: function(args,gui){
            var question = "Use ";
            question = (!_.has(args,"t1")) ? "what" : args.t1.getName();
            question+=" with "
            question = (!_.has(args,"t2")) ? "what" : args.t2.getName();
            question+="?"
            gui.showText(question);
        }
    },
    lookThing: {
        name: "Look at Something",
        category: "Things",
        description: "Show thing description",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.canReach(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("You don't see any "+originals.t);
                }
            }    
        ],
        syntax: ["look <t:Thing>", "look at <t:Thing>", "examine <t:Thing>"],
        execute: function(args,gui) {
            gui.showText(args.t.description.get());
        },
        executePartial: function(args,gui){
            gui.showText("Look what?");
        }
    },
    lookCharacter: {
        name: "Look at Someone",
        category: "Characters",
        description: "Show character description",
        restrictions: [
            {
                condition: function(args){
                    return !(args.c.isInLocation(Imaginaria.story.playerCharacter.location))
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't see "+originals.c);
                }
            }    
        ],
        syntax: ["look <c:Character>", "look at <c:Character>", "examine <c:Character>"],
        execute: function(args,gui) {
            gui.showText(args.c.description.get());
        },
        executePartial: function(args,gui){
            gui.showText("Look what?");
        }
    },
    /*
    goInside : {
        name: "Go Inside",
        category: "movement",
        description: "Moves the player character inside a container",
        restrictions: [
            {
                condition: function(args){
                    return !(args.t.isInLocation(Imaginaria.story.playerCharacter.location))
                },
                action: function(args,originals,gui) {
                    gui.showText("You don't see any "+originals.t);
                }
            },
            {
                condition: function(args){
                    return !(args.t.properties.container && args.t.properties.canContainCharacters);
                },
                action: function(args,originals,gui){
                    gui.showText("You can not get into "+args.t.getName());
                }
            },
            {
                condition: function(args){
                    return Imaginaria.story.playerCharacter.isInside(args.t);
                },
                action: function(args,originals,gui){
                    gui.showText("You are already inside "+args.t.getName());
                }
            } 
        ],
        syntax: ["move into <t:Thing>","go inside <t:Thing>","get in <t:Thing>","get into <t:Thing>","enter <t:Thing>"],
        execute: function(args,gui){
            Imaginaria.story.playerCharacter.getInside(args.t);
            gui.showText("You enter "+args.t.getName());
        }
    },
    goOutside : {
        name: "Go Outside",
        category: "movement",
        description: "Moves the player character outside a container",
        restrictions: [
            {
                condition: function(args){
                    return !(Imaginaria.story.playerCharacter.isInside(args.t));
                },
                action: function(args,originals,gui) {
                    gui.showText("You are not inside "+args.t.getName());
                }
            }
        ],
        syntax: [
            "stand out of <t:Thing>",
            "stand out <t:Thing>",
            "move out of <t:Thing>",
            "move out <t:Thing>",
            "go out of <t:Thing>",
            "get out <t:Thing>",
            "exit <t:Thing>",
            "leave <t:Thing>"
        ],
        execute: function(args,gui){
            delete args.t.properties.isContaining[Imaginaria.story.playerCharacter.key];
            Imaginaria.story.playerCharacter.location = _.clone(args.t.location);
            gui.showText("You get out of "+args.t.getName());
        }
    },
    getOnTop : {
        name: "Get On Top",
        category: "movement",
        description: "Moves the player character on top of a surface",
        restrictions: [
            {
                condition: function(args){
                    return !(args.t.isInLocation(Imaginaria.story.playerCharacter.location))
                },
                action: function(args,originals,gui) {
                    gui.showText("You don't see any "+originals.t);
                }
            },
            {
                condition: function(args){
                    return !(args.t.properties.supporter && args.t.properties.canHoldCharacters);
                },
                action: function(args,originals,gui){
                    gui.showText("You can not get on top of "+args.t.getName());
                }
            },
            {
                condition: function(args){
                    return Imaginaria.story.playerCharacter.isOnTopOf(args.t);
                },
                action: function(args,originals,gui){
                    gui.showText("You are already on top of "+args.t.getName());
                }
            } 
        ],
        syntax: [
            "climb <t:Thing>",
            "clamber up <t:Thing>",
            "go on <t:Thing>",
            "get on <t:Thing>",
            "move on <t:Thing>",
            "walk on <t:Thing>",
            "get on top of <t:Thing>",
        ],
        execute: function(args,gui){
            Imaginaria.story.playerCharacter.getOnTopOf(args.t);
            gui.showText("You get on top of "+args.t.getName());
        }
    },*/
    
    /*
    putIn:{
        syntax: [
            "put <t1:Thing> in <t2:Thing>",
            "get <t1:Thing> in <t2:Thing>",
            "put <t1:Thing> inside <t2:Thing>",           
            "put <t1:Thing> inside of <t2:Thing>"          
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" inside the "+args.t2;
        }
    },
    putOn:{
        syntax: [                
            "put <t1:Thing> on top of <t2:Thing>",
            "put <t1:Thing> on <t2:Thing>",
            "put <t1:Thing> over <t2:Thing>",
            "place <t1:Thing> on top of <t2:Thing>",
            "place <t1:Thing> on <t2:Thing>",
            "place <t1:Thing> over <t2:Thing>",         
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    push: {
        syntax: [                
            "push <t:Thing>",
            "shove <t:Thing>",
            "press <t:Thing>"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    pull: {
        syntax: [                
            "tug <t:Thing>",
            "pull <t:Thing>"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    }
    switch:{
        syntax: [                
            "switch <t:Thing>",
            "turn <t:Thing>",
            "flick <t:Thing>"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    switchOn:{
        syntax: [                
            "switch on <t:Thing>",
            "switch <t:Thing> on",
            "turn <t:Thing> on",
            "turn on <t:Thing>",
            "flick on <t:Thing>"
            "flick <t:Thing> on"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    switchOff:{
        syntax: [                
            "switch off <t:Thing>",
            "switch <t:Thing> off",
            "turn <t:Thing> off",
            "turn off <t:Thing>",
            "flick off <t:Thing>"
            "flick <t:Thing> off"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },*/
    /*
    lock: {
        syntax: ["lock <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    unlock: {
        syntax: ["unlock <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    lockWith:{
        syntax: ["close <t1:Thing> using <t2:Thing>","close <t1:Thing> with <t2:Thing>","lock <t1:Thing> with <t2:Thing>","lock <t1:Thing> using <t2:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    unlockWith: {
        syntax: ["unlock <t1:Thing> using <t2:Thing>","unlock <t1:Thing> with <t2:Thing>","open <t1:Thing> with <t2:Thing>","open <t1:Thing> using <t2:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    unlockWith:{
        syntax: ["unlock <t1:Thing> with <t2:Thing>","unlock <t1:Thing> using <t2:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },*/
    /*
    read: {
        name: "Read Something",
        category: "Things",
        description: "Show readable thing's read text",
        restrictions: [
            {                
                condition: function(args){
                    return !(args.t.properties.readable)
                },
                action: function(args,originals,gui) {
                    gui.showText("You can't read "+args.t.getName());
                }
            }  
        ],
        syntax: ["read <t:Thing>"],
        execute: function(args,gui) {
            gui.showText("You read "+args.t.getName());
            gui.showText(args.t.properties.textWhenRead);
        },
    },
    
    putOnClothing: {
        syntax: [                
            "wear <t:Thing>",
            "put on <t:Thing>",
            "put <t:Thing> on",
            "don <t:Thing>"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    takeOffClothing: {
        syntax: [                
            "remove <t:Thing>",
            "take off <t:Thing>",
            "take <t:Thing> off"
        ],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    drink: {
        syntax: ["drink <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    eat: {
        syntax: ["eat <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    blow: {
        syntax: ["blow <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    buy: {
        syntax: ["buy <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    cut: {
        syntax: ["cut <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    rub: {
        syntax: ["rub <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    search: {
        syntax: ["search <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    smell: {
        syntax: ["smell <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    squeeze: {
        syntax: ["squeeze <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    touch: {
        syntax: ["touch <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    turn: {
        syntax: ["turn <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    flip: {
        syntax: ["flip <t:Thing>"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    jump: {
        syntax: ["jump"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    listen: {
        syntax: ["listen"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    sing: {
        name: "Sing",
        category: "Misc",
        description: "Sing a little song",
        restrictions: [],
        syntax: ["sing"],
        execute: function(args,gui) {
            gui.showText("You sing a little song");
            gui.showText("Nothing happens");
        }
    },
    sleep: {
        syntax: ["sleep"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    think: {
        syntax: ["think"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    wakeUp: {
        syntax: ["wake up","awake"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    wave: {
        syntax: ["wave"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    },
    whistle: {
        syntax: ["whistle"],
        logic: function(args){
            return "You try to put the "+args.t1+" on top of the "+args.t2;
        }
    } */
}