var storyElementTypes = {
    Thing : {
        list:"things",
        class:Thing,
        hasReferences: true
    },
    Character : {
        list:"characters",
        class:Character,
        hasReferences: true
    },
    Place : {
        list:"places",
        class:Place,
        hasReferences: true
    },
    Command: {
        list:"commands",
        class:Command,
        hasReferences: false
    },
    SpecificCommand: {
        list:"specificCommands",
        class:SpecificCommand,
        hasReferences: false
    },
    Group: {
        list:"groups",
        class:Group,
        hasReferences:false
    }
}


function Story(){
    
    this.places = {};
    this.characters = {};
    this.things = {};    
    this.commands = {};
    this.specificCommands = {};
    this.events = {};
    this.groups = {};
    this.properties = {};

    this.playerCharacter = null;

    this.find = function(type,designation){
        var designationRegExp = new RegExp(designation,"i");
        return _.find(this[storyElementTypes[type].list],function(element){
            return element.isDesignated(designationRegExp);
        });
    }

    this.getElement = function(type,key){
        return this[storyElementTypes[type].list][key];
    }

    this.solveReference = function(reference){
        return this[storyElementTypes[reference.type].list][reference.key];
    }

    this.initElements = function(elementType,elementList){        
        _.each(elementList, function(elementMeta,key){
            var storyElement = storyElementTypes[elementType]; 
            this[storyElement.list][key] = new storyElement.class(key,elementMeta);
        },this);
    }

    this.initEvents = function(events){
        _.each(events,function(event,key){
            this.events[key] = new Event(key,event);
        },this);
    }

    this.allowedDirection = function(direction){
        return (this.playerCharacter.location.context == "Place" && this.playerCharacter.location.actualLocation.hasDirection(direction));
    }

    this.loadStory = function(story){
        this.initEvents(story.events);
        _.each(storyElementTypes, function(storyElement,key){
            this.initElements(key,story[storyElement.list]);
        },this);
        _.each(storyElementTypes, function(storyElement,key){
            if (storyElement.hasReferences){
                _.each(this[storyElement.list], function(element,key){
                    element.solveReferences();
                },this);    
            }            
        },this);
        this.playerCharacter = this.characters[story.playerCharacter];
        this.playerCharacter.playerCharacter = true;
    }
}

