function DynamicDescription(descriptionMeta){
    this.unconditional = descriptionMeta.unconditional;
    this.conditional = !descriptionMeta.conditional ? [] : descriptionMeta.conditional;

    this.get = function(){
        var desc = _.find(this.conditional,function(description){
            return description.condition();
        });
        return !desc ? this.unconditional : desc.description;
    }

}