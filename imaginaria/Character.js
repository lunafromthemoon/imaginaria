function Character(key,characterMeta,customProperties){
    
    this.setMetaInfo(key,characterMeta);
    this.setName();
    this.name.properName = characterMeta.properName;
    this.setDescription();
    this.setGroups();
    this.setProperties(defaults.characterProperties,characterMeta.customProperties);
    this.inventory = new Inventory({type:"Character",key:this.key},this.inventory);

    this.isDesignated = function(designation){
        //for the parser to find it
        return designation.test(this.name.properName) 
            || designation.test(this.name.noun) 
            || designation.test(this.name.partialName)
            || (this.playerCharacter && (designation.test("yourself") || designation.test("myself")));        
    }

    this.move = function(direction){
        this.location.actualLocation = this.location.actualLocation.getLocationTo(direction);
    }

    this.knows = function(thing){
        return this.thingsSeen[thing.key] == true;
    }

    this.canReach = function(thing){
        return this.knows(thing)
            && (this.isInLocation(thing.location) 
                || this.inventory.has(thing));
    }

}

function Inventory(owner,items){
    this.items = {};
    this.owner = owner;
    if (items){
        _.each(items,function(item){
            this.items[item.key] = item;
        },this);
    }

    this.has = function(thing){
        return _.has(this.items,things.key);
    }

    this.add = function(thing){
        this.items[thing.key] = thing;
        thing.getHeldBy(this.owner);
    }

    this.remove = function(thing){
        delete this.items[thing.key];
        thing.location = _.clone(Imaginaria.story.playerCharacter.location);
    }

    this.count = function(thing){
        _.keys(this.items).length;
    }

    this.list = function(){
        var names = _.map(this.items,function(thing){
            return thing.getName();
        });       
        return Imaginaria.utils.joinList(names);
    }
}

extend(Character.prototype, HasMetaInfo);
extend(Character.prototype, HasComplexName);
extend(Character.prototype, HasDynamicDescription);
extend(Character.prototype, HasProperties);
extend(Character.prototype, HasReferences);
extend(Character.prototype, HasGroups);
extend(Character.prototype, HasLocation);