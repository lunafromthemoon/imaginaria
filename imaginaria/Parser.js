

/*var command = "use the pink key qwer the big door";
      var pattern = /use ([\w\W\s]+) with ([\w\W\s]+)/i;
      var objects = command.replace(pattern,'{"o1":"$1", "o2":"$2"}');
      console.log(objects);
      objects = JSON.parse(objects);
      console.log(objects);*/

function Parser() {

  this.extractPatterns = function(syntaxMeta){
    return _.map(syntaxMeta,function(syntax){
        var lexemes = [];
        var params = [];
        var words = syntax.split(" ");
        _.each(words,function(word){
          if (word.indexOf(":") != -1){
            lexemes.push("([\\w\\W\\s]+)");
            var param = word.split(":");
            params.push({tag:param[0].substring(1),type:param[1].substring(0,param[1].length-1)});
          } else {
            lexemes.push(word);
          }
        });
        var pattern = new RegExp("^"+lexemes.join(" ")+"$","i");
        return {originalSyntax:syntax,pattern:pattern,params:params, paramsString:this.generateParamString(params)}
      },this);
  };

  this.generateParamString = function(params){
    //generates a string to parse a JSON from the regexp params 
    //e.g. '{"o1":"$1", "o2":"$2"}
    return "{" + _.reduce(params,function(memo,param,index) {
      return (memo == "" ? "" : ",") + '"' + param.tag + '":"$'+(index+1) + '"';
    }, "") + "}";
  }

  this.cleanShellExtras = function(commandString){
    var clean = commandString.replace(/ the /ig," ");
    clean = clean.replace(/ a /ig," ");
    clean = clean.replace(/ an /ig," ");
    clean = clean.replace(/^\s+|\s+$/g,''); //leading and trailing whitespaces
    clean = clean.replace(/\s\s/g," "); // two whitespaces
    return clean;
  }

  this.parseElements = function(commandString,syntax){
    var elements = {references: {}, originals: {}};
    if (syntax.params.length == 0) return elements;
    var paramsString = commandString.replace(syntax.pattern,syntax.paramsString);
    var values = JSON.parse(paramsString);
    //try to find objects matching the string parameters on the story
    _.each(syntax.params,function(param){
      var element = null;
      elements.originals[param.tag] = values[param.tag];
      if (param.type == "Text"){
        element = values[param.tag];
      } else if (param.type == "Direction") {
        if (Imaginaria.story.allowedDirection(values[param.tag])){
          element = values[param.tag];
        }
      } else {
        element = Imaginaria.story.find(param.type,values[param.tag]);  
      }

      
      if (element) {
        elements.references[param.tag] = element;  
      }      
    },this);
    return elements;
  }

  this.matches = function(commandString, command){
    var lastMatch = null;
    _.find(command.syntax,function(syntax){
      if (syntax.pattern.test(commandString)) {
        console.log("Found potential match");        
        var match = {command:command,syntax:syntax};
        var parsedElements = this.parseElements(commandString,syntax)
        match.elements = parsedElements.references;
        match.originalParams = parsedElements.originals;

        if (_.keys(match.elements).length < syntax.params.length) {
          //partial match, the sentence makes sense, but one or more objects are not recognized
          //e.g. open the door with the key, but there are no door or key objects in your story
          match.partial = true;
        } else {
          //full match, the game fully knows what you want to do
          match.full = true;
        }
        console.log(match);
        lastMatch = (lastMatch == null || match.full) ? match : lastMatch;       
        return lastMatch.full;
      }
      return false;
    },this);
    console.log(lastMatch);
    return lastMatch;
  }

  this.parseCommand = function(commandString){
    var cleanCommandString = this.cleanShellExtras(commandString);
    var lastMatch = null;
    _.find(Imaginaria.story.commands,function(command){
      var match = this.matches(cleanCommandString,command)
      if (match != null){
        lastMatch = match;
        if (lastMatch.full){
          return true;
        }
      }
      return false;
    },this);
    return lastMatch;
  }
}